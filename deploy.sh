#!/bin/bash

# Variables
PROJECT_ID=enduring-tea-423217-k5
TOPIC_NAME=loadbqtopic
FUNCTION_NAME=hello_pubsub_v2
ENTRY_POINT=process_pubsub_eventv2
REGION=us-central1
RUNTIME=python39

# Créer un sujet Pub/Sub
gcloud pubsub topics create $TOPIC_NAME

# Déployer la Cloud Function
gcloud functions deploy $FUNCTION_NAME \
  --region $REGION \
  --runtime $RUNTIME \
  --trigger-topic $TOPIC_NAME \
  --entry-point $ENTRY_POINT
