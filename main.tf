terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.34.0"
    }
  }
}

provider "google" {
  project = "enduring-tea-423217-k5"
  region  = "us-central1"
}

# Création d'un nouveau service account
resource "google_service_account" "pubsub_tobq_loader" {
  account_id   = "pubsub-tobq-loader"
  display_name = "Pub/Sub to BigQuery Loader"
}

# Ajout des rôles nécessaires au service account
resource "google_project_iam_member" "pubsub_tobq_loader_roles" {
  for_each = toset([
    "roles/cloudfunctions.invoker",
    "roles/bigquery.dataEditor",
    "roles/bigquery.jobUser",
    "roles/storage.objectViewer",
    "roles/cloudfunctions.developer",
    "roles/run.admin",
    "roles/eventarc.admin"
  ])

  project = "enduring-tea-423217-k5"
  role    = each.key
  member  = "serviceAccount:${google_service_account.pubsub_tobq_loader.email}"
}

# Création d'un dataset BigQuery
resource "google_bigquery_dataset" "dataset" {
  dataset_id = "pubsub_dataset_v2"
  project    = "enduring-tea-423217-k5"
  location   = "us-central1"
}

# Création d'une table BigQuery dans le dataset
resource "google_bigquery_table" "table" {
  table_id   = "pubsub_table_v2"
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  project    = "enduring-tea-423217-k5"
  deletion_protection = false
  schema     = <<EOF
[
  {
    "name": "firstname",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "lastname",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "age",
    "type": "INT64",
    "mode": "NULLABLE"
  }
]
EOF
}

# Créer un sujet Pub/Sub
resource "google_pubsub_topic" "load_to_bq_topic" {
  name = "load_to_bq_topic"
}

# Créer un bucket de stockage pour le code source de la Cloud Function
resource "google_storage_bucket" "bucket" {
  name     = "my-clf-source-bucket"
  location = "us-central1"
}

# Télécharger l'archive zip vers le bucket
resource "google_storage_bucket_object" "function_zip" {
  name   = "function-source.zip"
  bucket = google_storage_bucket.bucket.name
  source = "${path.module}/function-source.zip"
}

# Créer une Cloud Function pour Pub/Sub
resource "google_cloudfunctions2_function" "function" {
  depends_on = [
    google_project_iam_member.pubsub_tobq_loader_roles,
  ]
  name        = "pubsub_eventv2"
  location    = "us-central1"
  description = "A function triggered by Pub/Sub to load data into BigQuery"

  build_config {
    runtime     = "python39"
    entry_point = "process_pubsub_eventv2"
    environment_variables = {
      BUILD_CONFIG_TEST = "build_test"
    }
    source {
      storage_source {
        bucket = google_storage_bucket.bucket.name
        object = google_storage_bucket_object.function_zip.name
      }
    }
  }

  service_config {
    max_instance_count = 3
    min_instance_count = 1
    available_memory   = "256M"
    timeout_seconds    = 60
    environment_variables = {
      SERVICE_CONFIG_TEST = "config_test",
      GCP_PROJECT         = "enduring-tea-423217-k5",
      BQ_DATASET          = google_bigquery_dataset.dataset.dataset_id,
      BQ_TABLE            = google_bigquery_table.table.table_id
    }
    ingress_settings               = "ALLOW_ALL"
    all_traffic_on_latest_revision = true
    service_account_email          = google_service_account.pubsub_tobq_loader.email
  }

  event_trigger {
    event_type     = "google.cloud.pubsub.topic.v1.messagePublished"
    pubsub_topic   = google_pubsub_topic.load_to_bq_topic.id
    retry_policy   = "RETRY_POLICY_RETRY"
  }
}
