import json
from google.cloud import pubsub_v1

# Configuration
project_id = "enduring-tea-423217-k5"
topic_id = "load_to_bq_topic"
file_path = "./message.json"

# Initialize Pub/Sub client
publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(project_id, topic_id)

# Read and publish each line in the JSON file
with open(file_path, 'r') as file:
    for line in file:
        message_data = json.loads(line)
        message_json = json.dumps(message_data)
        message_bytes = message_json.encode('utf-8')

        # Publish the message
        future = publisher.publish(topic_path, data=message_bytes)
        print(f"Published message ID: {future.result()}")

print(f"All messages published to {topic_id}.")
