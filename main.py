import base64
import json
from google.cloud import bigquery
import os

def process_pubsub_eventv2(event, context):
    try:
        # Décode le message Pub/Sub
        pubsub_message = base64.b64decode(event['data']).decode('utf-8')
        print(f"Decoded message: {pubsub_message}")
        data = json.loads(pubsub_message)
        print(f"Parsed JSON: {data}")

        # Initialize BigQuery client
        client = bigquery.Client()
        dataset_id = os.environ.get('BQ_DATASET')
        table_id = os.environ.get('BQ_TABLE')
        project_id = os.environ.get('GCP_PROJECT')

        # Build the table reference
        table_ref = client.dataset(dataset_id, project=project_id).table(table_id)

        # Insert data into BigQuery
        errors = client.insert_rows_json(table_ref, [data])

        if errors == []:
            print(f"New row has been added to {table_id}")
        else:
            print(f"Encountered errors while inserting rows: {errors}")
    except json.JSONDecodeError as e:
        print(f"JSONDecodeError: {e}")
    except Exception as e:
        print(f"Error: {e}")
